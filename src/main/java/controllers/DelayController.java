package controllers;

import models.Delay;
import org.springframework.web.client.RestTemplate;

public class DelayController {

    private RestTemplate restTemplate;

    public DelayController() {
        restTemplate = new RestTemplate();
    }

    private final String HTTPBIN_API_BASE = "https://httpbin.org/delay";

    public Delay getDelayById(Integer delayId) {
        try {
            return restTemplate.getForObject(HTTPBIN_API_BASE + "/" + delayId, Delay.class);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
