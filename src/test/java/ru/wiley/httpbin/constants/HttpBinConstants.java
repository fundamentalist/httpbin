package ru.wiley.httpbin.constants;

public class HttpBinConstants {

    public static final String HTTPBIN_API_BASE = "https://httpbin.org/delay";
    public static final String HTTPBIN_IMAGE_URL = "https://httpbin.org/image/png";

    public static final String CHROME_DRIVER_PATH = "src/main/resources/drivers/v2.43/windows/chromedriver.exe";

}
