package ru.wiley.httpbin.tests;

import controllers.DelayController;
import io.restassured.RestAssured;
import models.Delay;
import org.json.JSONException;
import org.junit.Test;
import ru.wiley.httpbin.constants.HttpBinConstants;

import static org.junit.Assert.*;

public class DelayTest {

    private Integer delayId = 1;

    @Test
    public void checkDelay() throws JSONException {
        RestAssured.given().when().get(HttpBinConstants.HTTPBIN_API_BASE + "/" + delayId).then().statusCode(200);
    }

    @Test
    public void testDelayInfoResponse() {
        DelayController delayController = new DelayController();
        Delay remoteDelay = delayController.getDelayById(delayId);

        assertTrue(remoteDelay.getUrl().contains(HttpBinConstants.HTTPBIN_API_BASE + "/" + delayId ));
    }
}
