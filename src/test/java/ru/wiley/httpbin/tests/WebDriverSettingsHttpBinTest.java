package ru.wiley.httpbin.tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

import static ru.wiley.httpbin.constants.HttpBinConstants.CHROME_DRIVER_PATH;

public class WebDriverSettingsHttpBinTest {

    public ChromeDriver driver;

    @Before
    public void openWebPortalPage() {
        ChromeDriverService service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(CHROME_DRIVER_PATH))
                .usingAnyFreePort()
                .build();

        DesiredCapabilities caps = DesiredCapabilities.chrome();
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions );
        chromeOptions.merge(caps);
        driver = new ChromeDriver(service, chromeOptions);
        driver.manage().window().fullscreen();
    }

    @After
    public void closeWebPortalPage() {
        driver.quit();
    }
}
