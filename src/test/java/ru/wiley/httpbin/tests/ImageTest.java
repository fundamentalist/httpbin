package ru.wiley.httpbin.tests;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import ru.wiley.httpbin.pages.ImagePage;

import static ru.wiley.httpbin.constants.HttpBinConstants.HTTPBIN_IMAGE_URL;

public class ImageTest extends WebDriverSettingsHttpBinTest {

    @Test
    public void checkImage() {
        driver.get(HTTPBIN_IMAGE_URL);
        ImagePage imagePage = PageFactory.initElements(driver, ImagePage.class);
        imagePage.waitImage();
    }
}
