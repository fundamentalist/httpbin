package ru.wiley.httpbin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ImagePage {

    private By imageLocator = By.tagName("img");

    private WebDriver driver;
    private WebDriverWait wait;

    public ImagePage(){
    }

    public ImagePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void waitImage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(imageLocator));
    }
}
