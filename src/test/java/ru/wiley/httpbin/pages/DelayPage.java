package ru.wiley.httpbin.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DelayPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public DelayPage(){
    }

    public DelayPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }
}
